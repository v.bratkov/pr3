<?php
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
  if (!empty($_GET['save'])) {
    // Если есть параметр save, то выводим сообщение пользователю.
    print('Спасибо, результаты сохранены.');
  }
  // Включаем содержимое файла form.php.
  include('form.php');
  // Завершаем работу скрипта.
  exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

// Проверяем ошибки.
$errors = FALSE;
if (empty($_POST['name'])) {
  print('Заполните поле Фио.<br/>');
  $errors = TRUE;
}

if (empty($_POST['email'])){
  print ('Заполните поле email.<br>');
  $errors = true;
}
if (empty($_POST['superpowers'])){
  print ('Выберите хотя бы одну сверхспособность.<br>');
  $errors = true;

} else {
  $powers = serialize($_POST['superpowers']);
}
if (empty($_POST['biography'])){
  print ('Заполните поле биография<br>');
  $errors = true;
}
if (empty($_POST['know_check'])){
  print ('Потвердите ознакомление с контрактом<br>');
  $errors = true;
}
if (empty($_POST['date1'])){
  print ('Выберите дату рождения <br>');
  $errors = true;
}
if (empty($_POST['member_num'])){
  print ('Выберите число конечностей <br>');
  $errors = true;
}
if (empty($_POST['gender'])){
  print ('Выберите пол <br>');
  $errors = true;
}
if ($errors) {
  // При наличии ошибок завершаем работу скрипта.
  exit();
}

// Сохранение в базу данных.

$user = 'u20495'; $pass = '1204499';
    
$db = new PDO('mysql:host=localhost;dbname=u20495', $user, $pass, array(
PDO::ATTR_PERSISTENT => true
));
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

try {
    $stmt = $db->prepare("INSERT INTO table1 SET name = ?, email = ?, date = ?, sex = ?, limbs = ?, powers = ?, bio = ?");
    $stmt->execute(array(
        $_POST['name'],
        $_POST['email'],
        $_POST['date1'],
        $_POST['gender'],
        $_POST['member_num'],
        $powers,
        $_POST['biography'],
    ));
}
catch(PDOException $e) {
    print ('Ошибка: ' . $e->getMessage());
    exit();
}


//  stmt - это "дескриптор состояния".
 
//  Именованные метки.
//$stmt = $db->prepare("INSERT INTO test (label,color) VALUES (:label,:color)");
//$stmt -> execute(array('label'=>'perfect', 'color'=>'green'));
 
//Еще вариант
/*$stmt = $db->prepare("INSERT INTO users (firstname, lastname, email) VALUES (:firstname, :lastname, :email)");
$stmt->bindParam(':firstname', $firstname);
$stmt->bindParam(':lastname', $lastname);
$stmt->bindParam(':email', $email);
$firstname = "John";
$lastname = "Smith";
$email = "john@test.com";
$stmt->execute();
*/

// Делаем перенаправление.
// Если запись не сохраняется, но ошибок не видно, то можно закомментировать эту строку чтобы увидеть ошибку.
// Если ошибок при этом не видно, то необходимо настроить параметр display_errors для PHP.
header('Location: ?save=1');
