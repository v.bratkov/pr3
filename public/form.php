<!DOCTYPE html>
<html lang="ru">

<head>
  <title>Web3/index</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="./style.css">
  <meta name="viewport" content="width=device-width">
</head>

<body>
  <div class="content">
    <form id="form" action="" method="POST">
      <h3>Форма</h3>
      <label>Имя:
        <input type="text" name='name'><br>
      </label>
      <label>E-mail:
        <input type="email" name="email"><br>
      </label>
      <label>Дата рождения:
        <input type="date" id="date1" name="date1"><br>
      </label>
      <label>Пол
        <input type="radio" value="M" name="gender">Мужской
      </label>
      <label>
        <input type="radio" value="W" checked="checked" name="gender">Женский<br>
      </label>
      <label>Количество конечностей
        <input type="radio" value="1" checked="checked" name="member_num">1
      </label>
      <label>
        <input type="radio" value="2" name="member_num">2
      </label>
      <label>
        <input type="radio" value="3" name="member_num"> 3
      </label>
      <label>
        <input type="radio" value="4" name="member_num"> 4<br>
      </label>
      <label for="superpowers">Сверхспособности<br>
        <select name="superpowers" multiple="multiple" id="superpowers">
          <option value="immortal"> Бессмертие </option>
          <option value="throughWalls">Прохождение сквозь стены</option>
          <option value="levitation"> Левитация </option>
        </select><br>
      </label>
      <label>Биография<br>
        <textarea name="biography" id="biography" cols="25" rows="7"></textarea><br>
      </label>
      <label>
        <input type="checkbox" name="know_check">С контрактом ознакомлен
      </label><br>
      <button name="submitted">Отправить</button>
    </form>
  </div>
</body>

</html>